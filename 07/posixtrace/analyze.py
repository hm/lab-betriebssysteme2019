#!/usr/bin/env python3

import babeltrace
import sys
import plotly.offline as py
import plotly.graph_objs as go
import plotly.figure_factory as ff
from plotly import tools
import re
import datetime
from attrdict import AttrDict

# get the trace path from the first command line argument
trace_path = sys.argv[1]

trace_collection = babeltrace.TraceCollection()

trace_collection.add_trace(trace_path, 'ctf')

thread_state = {}

trace_thread = {}
trace_mutex = {}
thread_names = {}

pattern = re.compile("lttng_ust_pthread:(.+)_(begin|end|req|acq)")
desc_postproc = re.compile("thread: 0x([a-f0-9]+)")

offset = 0

for event in trace_collection.events:
    tid = event["pthread_id"]
    if offset == 0:
        offset = event.timestamp
    m = pattern.match(event.name)
    if m:
        if m.group(2) in ("begin", "req"):
            thread_state[tid] = AttrDict(time=event.timestamp)
            if m.group(1) == "pthread_create":
                thread_state[tid].start_routine=event["start_routine"]
                thread_state[tid].attr=event["attr"]
                thread_state[tid].arg=event["arg"]
            elif m.group(1) == "pthread_join":
                thread_state[tid].thread=event["thread"]
            elif m.group(1) == "pthread_mutex_lock":
                thread_state[tid].mutex = event["mutex"]
            elif m.group(1) == "pthread_cond_wait":
                thread_state[tid].mutex = event["mutex"]
                thread_state[tid].cond = event["cond"]
                trace_mutex.setdefault(event["mutex"], []).append(AttrDict(action="unlock", thread=tid,
                                                                           time=event.timestamp - offset))
            elif m.group(1) == "pthread_cond_signal":
                thread_state[tid].cond = event["cond"]
        elif tid in thread_state:
            thread = thread_state[tid]
            begin = thread.time - offset
            end = event.timestamp - offset
            desc = ""
            if m.group(1) == "pthread_create":
                desc = "(pthread_create) status: {}, thread: 0x{:016x}, start_routine: {}, arg: {}".format(
                    event["status"], event["thread"], thread.start_routine, thread.arg)
            elif m.group(1) == "pthread_join":
                desc = "(pthread_join) thread: 0x{:016x}, status: {}, retval: {:x}".format(thread.thread,
                    event["status"], event["retval"])
            elif m.group(1) == "pthread_mutex_lock":
                desc = "(pthread_mutex_lock) mutex: 0x{:x}, status: {}".format(thread.mutex, event["status"])
                trace_mutex.setdefault(thread.mutex, []).append(AttrDict(action="lock", thread=tid, time=end))
            elif m.group(1) == "pthread_cond_wait":
                desc = "(pthread_cond_wait) cond: 0x{:x}, mutex: 0x{:x}, status: {}".format(thread.cond,
                    thread.mutex, event["status"])
                trace_mutex.setdefault(thread.mutex, []).append(AttrDict(action="lock", thread=tid, time=end))
            elif m.group(1) == "pthread_cond_signal":
                desc = "(pthread_cond_signal) cond: 0x{:x}, status: {}".format(thread.cond, event["status"])
            section = dict(Task=tid, Start=begin, Finish=end, Function=m.group(1), Description=desc)
            trace_thread.setdefault(tid, []).append(section)
            del thread_state[tid]
    elif event.name == "lttng_ust_pthread:pthread_mutex_unlock":
        try:
            begin = thread_state[tid].time - offset
            end = event.timestamp - offset
            desc = "(pthread_mutex_unlock) mutex: 0x{:x}, status: {}".format(event["mutex"], event["status"])
            section = dict(Task=tid, Start=begin, Finish=end, Function="phtread_mutex_unlock", Description=desc)
            trace_thread.setdefault(tid, []).append(section)
            trace_mutex.setdefault(event["mutex"], []).append(AttrDict(action="unlock", thread=tid, time=begin))
            del thread_state[tid]
        except KeyError:
            pass
    elif event.name == "lttng_ust_pthread:pthread_setname_np":
        thread_names[event["thread"]] = event["name"]

df = []

for tid in trace_thread.keys():
    t = thread_names.get(tid, str(tid))
    for d in trace_thread[tid]:
        d['Task'] = "Thread {}".format(t)
        if d['Function'] in ("pthread_create", "pthread_join"):
            m = desc_postproc.search(d["Description"])
            if m:
                id = int(m.group(1), 16)
                if id in thread_names:
                    d["Description"] = d["Description"].replace("thread: 0x"+m.group(1), "thread: {}".format(thread_names[id]))
        df.append(d)

mutex_id = 0
for id in trace_mutex.keys():
    try:
        # Sanity check
        holder = None
        for event in trace_mutex[id]:
            if holder:
                if event.action != "unlock" or event.thread != holder:
                    raise RuntimeError()
                holder = None
            else:
                if event.action != "lock":
                    raise RuntimeError()
                holder = event.thread

        holder = None
        for event in trace_mutex[id]:
            if holder:
                holder["Description"] = "Holder: {}".format(thread_names.get(event.thread, str(event.thread)))
                holder["Finish"] = event.time
                df.append(holder)
                holder = None
            else:
                holder = dict(Task="Mutex {}".format(mutex_id), Start=event.time, Function="locked")
        mutex_id += 1
    except:
        pass

fig = ff.create_gantt(df, index_col='Function', show_colorbar=True, group_tasks=True, title="Thread Execution")
fig['layout']['width'] = None
fig['layout']['xaxis']['rangeselector']['buttons'] = None
fig['layout']['xaxis']['type'] = None
fig['layout']['yaxis']['fixedrange'] = True

py.plot(fig, filename='pthreads.html', config={"scrollZoom": True, "responsive": True})
