#!/bin/bash

if [ "$#" -ne 1 ]; then
    echo "Parameter missing. Usage: lttng.sh <program>"
    exit 1
fi

PTHREAD_TRACE_LIB=/tools/lttng-ust/lib/liblttng-ust-pthread-wrapper.so
lttng create --output trace
lttng enable-event --userspace 'lttng_ust_pthread:*'
lttng add-context --userspace --type pthread_id
lttng start
LD_PRELOAD=${PTHREAD_TRACE_LIB} $@
wait $!
lttng destroy
