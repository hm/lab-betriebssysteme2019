#include "measure.h"

#define TRACEPOINT_DEFINE
#include "tp.h"

#include <stdlib.h>

int main(int argc, char* argv[]) {
  int mycounter = rand();

  printf("counter: %d\n", mycounter);

  return 0;
}
